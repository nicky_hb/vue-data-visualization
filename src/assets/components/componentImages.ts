const images = {
  // 图表部分
  lineChart: require("@/assets/images/echarts/line-simple.webp?_v_=1612615474746"),
  barChart: require("@/assets/images/echarts/bar-background.webp?_v_=1612615474746"),
  pieChart: require("@/assets/images/echarts/pie-doughnut.webp?_v_=1612615474746"),
  radarChart: require("@/assets/images/echarts/radar.webp?_v_=1612615474746"),
  treeChart: require("@/assets/images/echarts/tree-basic.webp?_v_=1612615474746"),
  kChart: require("@/assets/images/echarts/custom-ohlc.webp?_v_=1612615474746"),
  gaugeChart: require("@/assets/images/echarts/gauge-simple.webp?_v_=1612615474746"),
  graphChart: require("@/assets/images/echarts/graph-simple.webp?_v_=1612615474746"),
  funnelChart: require("@/assets/images/echarts/funnel.webp?_v_=1612615474746"),

  // 标题
  normalTitle: "https://images.weserv.nl/?url=https://i0.hdslb.com/bfs/article/d8e3e470c0074b2b40766443f73bbd912b549fcb.png",
  subhead: "https://images.weserv.nl/?url=https://i0.hdslb.com/bfs/article/d8e3e470c0074b2b40766443f73bbd912b549fcb.png",
  headline: "https://images.weserv.nl/?url=https://i0.hdslb.com/bfs/article/d8e3e470c0074b2b40766443f73bbd912b549fcb.png",
  content: "https://images.weserv.nl/?url=https://i0.hdslb.com/bfs/article/d8e3e470c0074b2b40766443f73bbd912b549fcb.png",

  // 形状
  rectangle: require("@/assets/images/echarts/1.png"),
  circle: require("@/assets/images/echarts/2.png"),
  triangle: require("@/assets/images/echarts/3.png")
};

export default images;
